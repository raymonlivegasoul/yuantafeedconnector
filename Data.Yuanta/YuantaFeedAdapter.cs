using AxYuantaQuoteLib;
using java.util.logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Vegasoul.Data.Yuanta
{
	internal class YuantaFeedAdapter : YuantaAdapter
	{
		private List<string> subscription;

		private bool doResubscribe;

		public YuantaFeedAdapter(YuantaConfig config) : base(config)
		{
			this.subscription = new List<string>();
			this.yuantaQuoteAPI.OnMktStatusChange += new _DYuantaQuoteEvents_OnMktStatusChangeEventHandler(this.OnMktStatusChange);
			this.yuantaQuoteAPI.OnGetMktAll += new _DYuantaQuoteEvents_OnGetMktAllEventHandler(this.yuantaQuoteAPI_OnGetMktAll);
			this.yuantaQuoteAPI.OnRegError += new _DYuantaQuoteEvents_OnRegErrorEventHandler(this.yuantaQuoteAPI_OnRegError);
		}

		public void Close()
		{
			if (this.isActive)
			{
				this.isActive = false;
				this.doResubscribe = true;
			}
			YuantaAdapter.LOGGER.log(Level.INFO, "feed closed");
		}

		public void CloseAllEventHandler()
		{
			this.OnConnected = null;
			this.OnDisconnected = null;
			this.OnMarketData = null;
			this.OnRegError = null;
			this.yuantaQuoteAPI.Dispose();
		}

		public void Connect()
		{
			try
			{
				YuantaAdapter.LOGGER.log(Level.INFO, "Start to connect");
				this.yuantaQuoteAPI.SetMktLogon(this.config.UserId, this.config.Password, this.config.ServerIP, this.config.ServerPort, 1, 0);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				YuantaAdapter.LOGGER.log(Level.SEVERE, string.Concat("SetMktConnection failed:", exception.Message), exception);
			}
		}

		private void OnMktStatusChange(object sender, _DYuantaQuoteEvents_OnMktStatusChangeEvent e)
		{
			if (e.status == 2)
			{
				this.RaiseOnConnected(sender, e);
			}
			else if ((e.status == 1 ? false : e.msg[0] != '0'))
			{
				this.RaiseOnDisconnected(sender, e);
			}
		}

		protected virtual void RaiseOnConnected(object sender, _DYuantaQuoteEvents_OnMktStatusChangeEvent e)
		{
			if (this.OnConnected != null)
			{
				this.OnConnected(sender, e);
			}
		}

		protected virtual void RaiseOnDisconnected(object sender, _DYuantaQuoteEvents_OnMktStatusChangeEvent e)
		{
			if (this.OnDisconnected != null)
			{
				this.OnDisconnected(sender, e);
			}
		}

		public virtual void RaiseOnLoggedIn(int frontID, int sessionID)
		{
			if (this.doResubscribe)
			{
				this.doResubscribe = false;
				this.Subscribe(this.subscription.ToArray());
			}
		}

		public void RaiseOnMarketData(object sender, _DYuantaQuoteEvents_OnGetMktAllEvent e)
		{
			DepthMarketDataInfo depthMarketDataInfo = new DepthMarketDataInfo();
			depthMarketDataInfo.CopyFromYuantaEvent(e);
			DateTime now = DateTime.Now;
			now = now.AddHours((double)this.config.TimezoneShift);
			depthMarketDataInfo.TradingDay = string.Format("{0:yyyyMMdd}", now);
			depthMarketDataInfo.ExchangeID = "XTAF";
			//YuantaAdapter.LOGGER.log(Level.WARNING, string.Concat(new object[] { "Yuanta received trade:", depthMarketDataInfo.Symbol, ", price", depthMarketDataInfo.MatchPrice, ", size:", depthMarketDataInfo.MatchQty, ", total size:", depthMarketDataInfo.TotalMatchQty, ", time: ", depthMarketDataInfo.MatchTime }));
			if (this.OnMarketData != null)
			{
				this.OnMarketData(depthMarketDataInfo);
			}
		}

		private void RaiseOnRegError(object sender, _DYuantaQuoteEvents_OnRegErrorEvent e)
		{
			if (this.OnRegError != null)
			{
				this.OnRegError(sender, e);
			}
		}

		public void Subscribe(string[] symbols)
		{
			try
			{
				string[] strArrays = symbols;
				int num = 0;
				while (num < (int)strArrays.Length)
				{
					string str = strArrays[num];
					if (!this.subscription.Contains(str))
					{
						if (this.yuantaQuoteAPI.AddMktReg(str, this.config.UpdateMode, 1, 0) == 0)
						{
							this.subscription.Add(str);
						}
						num++;
					}
					else
					{
						return;
					}
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				YuantaAdapter.LOGGER.log(Level.SEVERE, string.Concat("AddMktReg failed:", exception.Message), exception);
			}
		}

		public void Unsubscribe(string[] symbols)
		{
			try
			{
				string[] strArrays = symbols;
				for (int i = 0; i < (int)strArrays.Length; i++)
				{
					string str = strArrays[i];
					this.yuantaQuoteAPI.DelMktReg(str, 1);
					this.subscription.Remove(str);
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				YuantaAdapter.LOGGER.log(Level.SEVERE, string.Concat("DelMktReg failed:", exception.Message), exception);
			}
		}

		private void yuantaQuoteAPI_OnGetMktAll(object sender, _DYuantaQuoteEvents_OnGetMktAllEvent e)
		{
			this.RaiseOnMarketData(sender, e);
		}

		private void yuantaQuoteAPI_OnRegError(object sender, _DYuantaQuoteEvents_OnRegErrorEvent e)
		{
			this.RaiseOnRegError(sender, e);
		}

		public event ConnectEventHandler OnConnected;

		public event DisconnectedEventHandler OnDisconnected;

		public event MarketDataInfoHandler OnMarketData;

		public event RegErrorEventHandler OnRegError;
	}
}