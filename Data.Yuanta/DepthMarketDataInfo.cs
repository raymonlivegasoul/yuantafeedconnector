using AxYuantaQuoteLib;
using System;

namespace Vegasoul.Data.Yuanta
{
	public class DepthMarketDataInfo
	{
		private string exchangeID;

		private string tradingDay;

		private string symbol;

		private double refPrice;

		private double openPrice;

		private double highPrice;

		private double lowPrice;

		private double upPrice;

		private double downPrice;

		private string matchTime;

		private double matchPrice;

		private double matchQty;

		private double totalMatchQty;

		private double bestBuyQty;

		private double bestBuyPrice;

		private double bestSellQty;

		private double bestSellPrice;

		public double BestBuyPrice
		{
			get
			{
				return this.bestBuyPrice;
			}
			set
			{
				this.bestBuyPrice = value;
			}
		}

		public double BestBuyQty
		{
			get
			{
				return this.bestBuyQty;
			}
			set
			{
				this.bestBuyQty = value;
			}
		}

		public double BestSellPrice
		{
			get
			{
				return this.bestSellPrice;
			}
			set
			{
				this.bestSellPrice = value;
			}
		}

		public double BestSellQty
		{
			get
			{
				return this.bestSellQty;
			}
			set
			{
				this.bestSellQty = value;
			}
		}

		public string ExchangeID
		{
			get
			{
				return this.exchangeID;
			}
			set
			{
				this.exchangeID = value;
			}
		}

		public double HighPrice
		{
			get
			{
				return this.highPrice;
			}
			set
			{
				this.highPrice = value;
			}
		}

		public double LowPrice
		{
			get
			{
				return this.lowPrice;
			}
			set
			{
				this.lowPrice = value;
			}
		}

		public double MatchPrice
		{
			get
			{
				return this.matchPrice;
			}
			set
			{
				this.matchPrice = value;
			}
		}

		public double MatchQty
		{
			get
			{
				return this.matchQty;
			}
			set
			{
				this.matchQty = value;
			}
		}

		public string MatchTime
		{
			get
			{
				return this.matchTime;
			}
			set
			{
				this.matchTime = value;
			}
		}

		public double OpenPrice
		{
			get
			{
				return this.openPrice;
			}
			set
			{
				this.openPrice = value;
			}
		}

		public double RefPrice
		{
			get
			{
				return this.refPrice;
			}
			set
			{
				this.refPrice = value;
			}
		}

		public string Symbol
		{
			get
			{
				return this.symbol;
			}
			set
			{
				this.symbol = value;
			}
		}

		public double TotalMatchQty
		{
			get
			{
				return this.totalMatchQty;
			}
			set
			{
				this.totalMatchQty = value;
			}
		}

		public string TradingDay
		{
			get
			{
				return this.tradingDay;
			}
			set
			{
				this.tradingDay = value;
			}
		}

		public double UpPrice
		{
			get
			{
				return this.upPrice;
			}
			set
			{
				this.upPrice = value;
			}
		}

		public DepthMarketDataInfo()
		{
		}

		public void CopyFrom(DepthMarketDataInfo source)
		{
			this.exchangeID = string.Copy(source.exchangeID);
			this.tradingDay = string.Copy(source.tradingDay);
			this.symbol = string.Copy(source.symbol);
			this.refPrice = source.refPrice;
			this.openPrice = source.openPrice;
			this.highPrice = source.highPrice;
			this.lowPrice = source.lowPrice;
			this.upPrice = source.upPrice;
			this.downPrice = source.downPrice;
			this.matchPrice = source.matchPrice;
			this.matchQty = source.matchQty;
			this.TotalMatchQty = source.TotalMatchQty;
			this.MatchTime = string.Copy(source.MatchTime);
			this.bestBuyPrice = source.bestBuyPrice;
			this.bestBuyQty = source.bestBuyQty;
			this.bestSellPrice = source.bestSellPrice;
			this.bestSellQty = source.bestSellQty;
		}

		public void CopyFromYuantaEvent(_DYuantaQuoteEvents_OnGetMktAllEvent e)
		{
			this.symbol = string.Copy(e.symbol);
			this.refPrice = double.Parse(e.refPri);
			this.openPrice = double.Parse(e.openPri);
			this.highPrice = double.Parse(e.highPri);
			this.lowPrice = double.Parse(e.lowPri);
			this.upPrice = double.Parse(e.upPri);
			this.downPrice = double.Parse(e.dnPri);
			this.matchPrice = double.Parse(e.matchPri);
			this.matchQty = double.Parse(e.matchQty);
			this.TotalMatchQty = double.Parse(e.tolMatchQty);
			this.MatchTime = string.Copy(e.matchTime);
			this.bestBuyPrice = double.Parse(e.bestBuyPri.Split(new char[] { ',' })[0]);
			this.bestBuyQty = double.Parse(e.bestBuyQty.Split(new char[] { ',' })[0]);
			this.bestSellPrice = double.Parse(e.bestSellPri.Split(new char[] { ',' })[0]);
			this.bestSellQty = double.Parse(e.bestSellQty.Split(new char[] { ',' })[0]);
		}

		public static DepthMarketDataInfo Create()
		{
			return new DepthMarketDataInfo();
		}
	}
}