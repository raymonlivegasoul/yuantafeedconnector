using deltix.data.stream;
using deltix.qsrv.hf.framework.feed;
using deltix.qsrv.hf.spi.feed;
//using java.lang;
using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace Vegasoul.Data.Yuanta
{
	public class FeedFactory : AbstractFeedProviderFactory
	{
		private string serverIP;

		private string serverPort;

		private string userId;

		private string password;

		private int timezoneShift;

		private int loginTimeout = 15000;

		private int initEventsBufferSize;

		private FeedProviderL1 provider;

		private ApplicationContext context;

		public FeedFactory()
		{
		}

		public override deltix.qsrv.hf.spi.feed.FeedProvider create(MessageChannel receiver, MessageChannel system)
		{
			if (this.provider != null)
			{
				this.provider.close();
				this.provider.adapter.CloseAllEventHandler();
				this.provider = null;
				Console.WriteLine("dispose provider");
			}
			if (this.context != null)
			{
				this.context.ExitThread();
			}
			Thread thread = new Thread(() => this.createFeed(receiver, system));
			thread.SetApartmentState(ApartmentState.STA);
			thread.Start();
			while (this.provider == null)
			{
			}
			return this.provider;
		}

		public void createFeed(MessageChannel receiver, MessageChannel system)
		{
			try
			{
                Console.WriteLine("serverIP={0}, serverPort={1}", serverIP, serverPort);
                serverPort = "80";
                serverIP = "172.22.18.62";
                this.provider = new FeedProviderL1(this.providerId, this.serverIP, this.serverPort, this.userId, this.password, this.timezoneShift, this.loginTimeout, this.initEventsBufferSize, receiver);
				this.context = new ApplicationContext();
				Application.Run(this.context);
			}
			catch (Exception exception)
			{
				throw new java.lang.RuntimeException(exception);
			}
		}

		public void setInitEventsBufferSize(int initEventsBufferSize)
		{
			this.initEventsBufferSize = initEventsBufferSize;
		}

		public void setLoginTimeout(int loginTimeout)
		{
			this.loginTimeout = loginTimeout;
		}

		public void setPassword(string password)
		{
			this.password = password;
		}

		public void setServerIP(string serverIP)
		{
			this.serverIP = serverIP;
		}

		public void setServerPort(string serverPort)
		{
			this.serverPort = serverPort;
		}

		public void setTimezoneShift(int timezoneShift)
		{
			this.timezoneShift = timezoneShift;
		}

		public void setUserId(string userId)
		{
			this.userId = userId;
		}
	}
}