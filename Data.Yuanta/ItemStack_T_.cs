using System;
using System.Collections.Generic;
using System.Threading;

namespace Vegasoul.Data.Yuanta
{
	internal class ItemStack<T>
	{
		private volatile int currentItem;

		private object lockObject;

		private ItemStack<T>.CreateItemDelegate createDelegate;

		private List<T> items;

		public ItemStack(ItemStack<T>.CreateItemDelegate createDelegate, int initialSize)
		{
			this.currentItem = -1;
			this.items = new List<T>();
			this.lockObject = new object();
			this.createDelegate = createDelegate;
			for (int i = 0; i < initialSize; i++)
			{
				this.items.Add(createDelegate());
			}
		}

		~ItemStack()
		{
			this.items.Clear();
		}

		public T GetItem()
		{
			T t;
			T item;
			object obj = this.lockObject;
			Monitor.Enter(obj);
			try
			{
				if (this.currentItem < 0)
				{
					item = this.createDelegate();
				}
				else
				{
					List<T> ts = this.items;
					int num = this.currentItem;
					this.currentItem = num - 1;
					item = ts[num];
				}
				t = item;
			}
			finally
			{
				Monitor.Exit(obj);
			}
			return t;
		}

		public void ReleaseItem(T element)
		{
			object obj = this.lockObject;
			Monitor.Enter(obj);
			try
			{
				int num = this.currentItem + 1;
				this.currentItem = num;
				if (num < this.items.Count)
				{
					this.items[this.currentItem] = element;
				}
				else
				{
					this.items.Add(element);
				}
			}
			finally
			{
				Monitor.Exit(obj);
			}
		}

		public delegate T CreateItemDelegate();
	}
}