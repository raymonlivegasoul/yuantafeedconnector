using AxYuantaQuoteLib;
using deltix.qsrv.hf.tickdb.pub;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Resources;
using System.Windows.Forms;

namespace Vegasoul.Data.Yuanta
{
	public class YuantaQuoteForm : Form, LoadingErrorListener
	{
		private bool loaded;

		private IContainer components = null;

		private AxYuantaQuote axYuantaQuote1;

		public bool Loaded
		{
			get
			{
				return this.loaded;
			}
		}

		public AxYuantaQuote YuantaQuoteAPI
		{
			get
			{
				return this.axYuantaQuote1;
			}
		}

		public YuantaQuoteForm()
		{
			this.InitializeComponent();
		}

		protected override void Dispose(bool disposing)
		{
			if ((!disposing ? false : this.components != null))
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(YuantaQuoteForm));
			this.axYuantaQuote1 = new AxYuantaQuote();
			((ISupportInitialize)this.axYuantaQuote1).BeginInit();
			base.SuspendLayout();
			this.axYuantaQuote1.Enabled = true;
			this.axYuantaQuote1.Location = new Point(38, 31);
			this.axYuantaQuote1.Name = "axYuantaQuote1";
			this.axYuantaQuote1.OcxState = (AxHost.State)componentResourceManager.GetObject("axYuantaQuote1.OcxState");
			this.axYuantaQuote1.Size = new System.Drawing.Size(100, 50);
			this.axYuantaQuote1.TabIndex = 0;
			base.AutoScaleDimensions = new SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(292, 273);
			base.Controls.Add(this.axYuantaQuote1);
			base.Name = "YuantaQuoteForm";
			this.Text = "YuantaQuoteForm";
			base.Load += new EventHandler(this.YuantaQuoteForm_Load);
			base.FormClosing += new FormClosingEventHandler(this.YuantaQuoteForm_FormClosing);
			((ISupportInitialize)this.axYuantaQuote1).EndInit();
			base.ResumeLayout(false);
		}

		public void Log(string type, string info)
		{
		}

		public void onError(LoadingError er)
		{
		}

		protected override void OnVisibleChanged(EventArgs e)
		{
			base.OnVisibleChanged(e);
			base.Visible = false;
		}

		private void YuantaQuoteForm_FormClosing(object sender, FormClosingEventArgs e)
		{
		}

		private void YuantaQuoteForm_Load(object sender, EventArgs e)
		{
			this.loaded = true;
		}
	}
}