using deltix.data.stream;
using deltix.qsrv.hf.pub;
using deltix.util.time;
using java.lang;
using java.util;
using System;
using System.Globalization;

namespace Vegasoul.Data.Yuanta
{
	internal sealed class FeedProviderL1 : FeedProvider
	{
		private BestBidOfferMessage bbo;

		private FeedProviderL1.BBOMessage currentBBO;

		private HashMap bboMap;

		public FeedProviderL1(string springID, string serverIP, string serverPort, string userID, string password, int timezoneShift, int loginTimeout, int itemsStackSize, MessageChannel receiver) : base(springID, serverIP, serverPort, userID, password, timezoneShift, loginTimeout, itemsStackSize, receiver)
		{
			this.bbo = new BestBidOfferMessage();
			this.currentBBO = new FeedProviderL1.BBOMessage();
			this.bboMap = new HashMap();
		}

		protected override bool HasSupportedType(InstrumentFilter iFilter)
		{
			return (iFilter.acceptMessageType(MarketMessage.Type.TYPE_TRADE) ? true : iFilter.acceptMessageType(MarketMessage.Type.TYPE_BBO));
		}

		protected override void OnQuote(DepthMarketDataInfo info)
		{
			this.currentBBO.Set(info.BestBuyPrice, info.BestBuyQty, info.BestSellPrice, info.BestSellQty);
			if (!this.currentBBO.IsEmpty)
			{
				HashMap hashMap = (HashMap)this.bboMap.@get(this.tradingDay);
				if (hashMap == null)
				{
					hashMap = new HashMap();
					this.bboMap.put(this.tradingDay.toString(), hashMap);
				}
				FeedProviderL1.BBOMessage copy = (FeedProviderL1.BBOMessage)hashMap.@get(this.symbol);
				if (copy != null)
				{
					if (!copy.Equals(this.currentBBO))
					{
						goto Label1;
					}
					return;
				}
				else
				{
					copy = this.currentBBO.GetCopy();
					hashMap.put(this.symbol.toString(), copy);
				}
			Label1:
				DateTime dateTime = DateTime.ParseExact(string.Concat(info.TradingDay, info.MatchTime), "yyyyMMddHHmmssffffff", CultureInfo.InvariantCulture, DateTimeStyles.None);
				dateTime = dateTime.AddHours(-8);
				this.bbo.originalTimestamp = DateConverter.ToLong(dateTime);
				this.bbo.symbol = this.symbol;
				this.bbo.bidPrice = this.currentBBO.BestBidPrice;
				this.bbo.bidSize = this.currentBBO.BestBidSize;
				this.bbo.offerPrice = this.currentBBO.BestAskPrice;
				this.bbo.offerSize = this.currentBBO.BestAskSize;
				this.bbo.bidExchange = this.exchangeCode;
				this.bbo.offerExchange = this.exchangeCode;
				this.bbo.instrumentType = this.instrumentType;
				copy.Set(this.currentBBO);
				this.receiver.send(this.bbo);
			}
		}

		private class BBOMessage
		{
			private double bestBidPrice;

			private double bestBidSize;

			private double bestAskPrice;

			private double bestAskSize;

			public double BestAskPrice
			{
				get
				{
					return this.bestAskPrice;
				}
				set
				{
					this.bestAskPrice = value;
				}
			}

			public double BestAskSize
			{
				get
				{
					return this.bestAskSize;
				}
				set
				{
					this.bestAskSize = value;
				}
			}

			public double BestBidPrice
			{
				get
				{
					return this.bestBidPrice;
				}
				set
				{
					this.bestBidPrice = value;
				}
			}

			public double BestBidSize
			{
				get
				{
					return this.bestBidSize;
				}
				set
				{
					this.bestBidSize = value;
				}
			}

			public bool IsEmpty
			{
				get
				{
					return (!double.IsNaN(this.bestBidPrice) || !double.IsNaN(this.bestBidSize) || !double.IsNaN(this.bestAskPrice) ? false : double.IsNaN(this.bestAskSize));
				}
			}

			public BBOMessage()
			{
			}

			public BBOMessage(double bestBidPrice, double bestBidSize, double bestAskPrice, double bestAskSize)
			{
				this.Set(bestBidPrice, bestBidSize, bestAskPrice, bestAskSize);
			}

			public override bool Equals(object obj)
			{
				bool flag;
				if ((obj == null ? false : obj is FeedProviderL1.BBOMessage))
				{
					FeedProviderL1.BBOMessage bBOMessage = obj as FeedProviderL1.BBOMessage;
					flag = (bBOMessage.bestAskPrice != this.bestAskPrice || bBOMessage.bestAskSize != this.bestAskSize || bBOMessage.bestBidPrice != this.bestBidPrice ? false : bBOMessage.bestBidSize == this.bestBidSize);
				}
				else
				{
					flag = false;
				}
				return flag;
			}

			public FeedProviderL1.BBOMessage GetCopy()
			{
				FeedProviderL1.BBOMessage bBOMessage = new FeedProviderL1.BBOMessage(this.bestBidPrice, this.bestBidSize, this.bestAskPrice, this.bestAskSize);
				return bBOMessage;
			}

			public void Set(FeedProviderL1.BBOMessage source)
			{
				if (source != null)
				{
					this.Set(source.bestBidPrice, source.bestBidSize, source.bestAskPrice, source.bestAskSize);
				}
			}

			public void Set(double bestBidPrice, double bestBidSize, double bestAskPrice, double bestAskSize)
			{
				if ((bestBidPrice != double.MaxValue ? true : bestBidSize != 0))
				{
					this.bestBidPrice = bestBidPrice;
					this.bestBidSize = bestBidSize;
				}
				else
				{
					this.bestBidPrice = double.NaN;
					this.bestBidSize = double.NaN;
				}
				if ((bestAskPrice != double.MaxValue ? true : bestAskSize != 0))
				{
					this.bestAskPrice = bestAskPrice;
					this.bestAskSize = bestAskSize;
				}
				else
				{
					this.bestAskPrice = double.NaN;
					this.bestAskSize = double.NaN;
				}
			}
		}
	}
}