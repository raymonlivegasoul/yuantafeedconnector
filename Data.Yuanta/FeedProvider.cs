using AxYuantaQuoteLib;
using deltix.data.stream;
using deltix.qsrv;
using deltix.qsrv.hf.framework.feed;
using deltix.qsrv.hf.pub;
using deltix.qsrv.hf.spi.conn;
using deltix.util.collections;
using deltix.util.concurrent;
using deltix.util.time;
//using java.lang;
using java.util;
using java.util.concurrent;
using java.util.logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace Vegasoul.Data.Yuanta
{
	internal abstract class FeedProvider : AbstractFeedProvider, Disconnectable
	{
		private bool tradeMessages;

		private bool bboMessages;

		private bool level2Messages;

		public YuantaFeedAdapter adapter;

		private YuantaConfig config;

		protected bool loggedIn;

		private bool firstAttempt;

		protected MessageChannel receiver;

		private TradeMessage trade;

		protected ByteArrayToCharSequence symbol;

		protected ByteArrayToCharSequence exchange;

		protected ByteArrayToCharSequence tradingDay;

		protected long exchangeCode;

		protected InstrumentType instrumentType;

		private readonly DisconnectableEventHandler disconnectableImpl;

		private readonly LinkedBlockingQueue msgQueue;

		private ItemStack<DepthMarketDataInfo> raw;

		private CharSequenceToObjectMap symbolTypes;

		private HashSet<string> subscription;

		private HashMap tradeMap;

		private Thread eventsThread;

		private FeedFilter filter;

		protected readonly static Logger LOGGER;

		static FeedProvider()
		{
			FeedProvider.LOGGER = Logger.getLogger("vegasoul.yuanta.provider");
		}

		public FeedProvider(string springID, string serverIP, string serverPort, string userID, string password, int timezoneShift, int loginTimeout, int itemsStackSize, MessageChannel receiver) : base(springID)
		{
			FeedProvider feedProvider = this;
			FeedProvider.LOGGER.info("create provider");
			this.firstAttempt = true;
			this.tradeMap = new HashMap();
			this.msgQueue = new LinkedBlockingQueue();
			this.raw = new ItemStack<DepthMarketDataInfo>(new ItemStack<DepthMarketDataInfo>.CreateItemDelegate(DepthMarketDataInfo.Create), itemsStackSize);
			this.symbolTypes = new CharSequenceToObjectMap();
			this.subscription = new HashSet<string>();
			this.disconnectableImpl = new DisconnectableEventHandler();
			this.receiver = receiver;
			this.config = new YuantaConfig(QSHome.@get(), serverIP, serverPort, userID, password, timezoneShift, loginTimeout);
			this.adapter = new YuantaFeedAdapter(this.config);
			this.adapter.OnConnected += new ConnectEventHandler((object sender, _DYuantaQuoteEvents_OnMktStatusChangeEvent e) => {
				feedProvider.loggedIn = true;
				feedProvider.firstAttempt = false;
				feedProvider.ApplyFilter();
				FeedProvider.LOGGER.info(string.Concat(springID, " provider connected"));
				if (!feedProvider.firstAttempt)
				{
					feedProvider.disconnectableImpl.onReconnected();
				}
			});
			this.adapter.OnRegError += new RegErrorEventHandler((object sender, _DYuantaQuoteEvents_OnRegErrorEvent e) => {
			});
			this.adapter.OnDisconnected += new DisconnectedEventHandler((object sender, _DYuantaQuoteEvents_OnMktStatusChangeEvent e) => {
				feedProvider.loggedIn = false;
				FeedProvider.LOGGER.log(Level.INFO, string.Concat(springID, " provider disconnected: {0} "), e.msg);
				feedProvider.disconnectableImpl.onDisconnected();
			});
			this.adapter.OnMarketData += new MarketDataInfoHandler(this.adapter_OnMarketData);
			this.trade = new TradeMessage();
			this.symbol = new ByteArrayToCharSequence();
			this.exchange = new ByteArrayToCharSequence();
			this.tradingDay = new ByteArrayToCharSequence();
			this.eventsThread = new Thread(new ThreadStart(this.ProcessEvents))
			{
				IsBackground = true,
				Name = string.Concat(springID, " Events Processor")
			};
			this.eventsThread.Start();
			this.adapter.Connect();
		}

		private void adapter_OnMarketData(DepthMarketDataInfo info)
		{
			DepthMarketDataInfo item = this.raw.GetItem();
			item.CopyFrom(info);
			this.msgQueue.@add(item);
		}

		public void addDisconnectEventListener(DisconnectEventListener listener)
		{
			this.disconnectableImpl.addDisconnectEventListener(listener);
		}

		private void ApplyFilter()
		{
			if (this.loggedIn)
			{
				try
				{
					FeedProvider feedProvider = this;
					Monitor.Enter(feedProvider);
					try
					{
						if ((this.filter == null ? false : !FeedProvider.IsFilterEmpty(this.filter)))
						{
							if (FeedProvider.LOGGER.isLoggable(Level.FINE))
							{
								FeedProvider.LOGGER.log(Level.FINE, string.Concat(this.getID(), " call .ExtractSymbols()"));
							}
							this.symbolTypes = this.ExtractSymbols(this.filter);
							string[] strArrays = (string[])this.symbolTypes.keySet().toArray(new string[0]);
							HashSet<string> strs = new HashSet<string>(this.subscription);
							strs.ExceptWith(strArrays);
							if (FeedProvider.LOGGER.isLoggable(Level.FINE))
							{
								FeedProvider.LOGGER.log(Level.FINE, string.Concat(this.getID(), " call wrapper's .Unsubscribe()"));
							}
							this.Unsubscribe(strs.ToArray<string>());
							this.ProcessMessageTypes(this.filter);
							if (FeedProvider.LOGGER.isLoggable(Level.FINE))
							{
								FeedProvider.LOGGER.log(Level.FINE, string.Concat(this.getID(), " call wrapper's .Subscribe()"));
							}
							this.Subscribe(strArrays);
							this.subscription = new HashSet<string>(strArrays);
						}
						else
						{
							if (FeedProvider.LOGGER.isLoggable(Level.FINE))
							{
								FeedProvider.LOGGER.log(Level.FINE, string.Concat(this.getID(), " call .UnsibscribeAll()"));
							}
							this.UnsubscribeAll();
							return;
						}
					}
					finally
					{
						Monitor.Exit(feedProvider);
					}
				}
				catch (Exception exception)
				{
					FeedProvider.LOGGER.log(Level.SEVERE, "Unable to set feed filter.", exception);
				}
			}
		}

		public override void close()
		{
			if (this.adapter != null)
			{
				this.adapter.Close();
			}
			this.raw = null;
		}

		private CharSequenceToObjectMap ExtractSymbols(FeedFilter filter)
		{
			CharSequenceToObjectMap charSequenceToObjectMap;
			CharSequenceToObjectMap charSequenceToObjectMap1 = new CharSequenceToObjectMap();
			if ((filter == null ? false : !filter.isRestricted()))
			{
				InstrumentType[] supportedInstruments = FeedFilter.getSupportedInstruments();
				for (int i = 0; i < (int)supportedInstruments.Length; i++)
				{
					InstrumentType instrumentType = supportedInstruments[i];
					InstrumentFilter instrumentFilter = filter.getFilter(instrumentType);
					if ((instrumentFilter == null || instrumentFilter.isRestricted() ? false : this.HasSupportedType(instrumentFilter)))
					{
						CharSequenceSet symbols = instrumentFilter.getSymbols();
						if ((symbols == null ? false : !symbols.isEmpty()))
						{
							foreach (string symbol in symbols)
							{
								charSequenceToObjectMap1.put(symbol, instrumentType);
							}
						}
					}
				}
				charSequenceToObjectMap = charSequenceToObjectMap1;
			}
			else
			{
				charSequenceToObjectMap = null;
			}
			return charSequenceToObjectMap;
		}

		private void HandleEvents(DepthMarketDataInfo info)
		{
			if ((this.tradeMessages ? true : this.bboMessages))
			{
				this.symbol.@set(Encoding.UTF8.GetBytes(info.Symbol), info.Symbol.Length);
				this.exchange.@set(Encoding.UTF8.GetBytes(info.ExchangeID), info.ExchangeID.Length);
				this.tradingDay.@set(Encoding.UTF8.GetBytes(info.TradingDay), info.TradingDay.Length);
				this.exchangeCode = (this.exchange.bytes[0] == 0 ? -9223372036854775808L : ExchangeCodec.codeToLong(this.exchange));
				this.instrumentType = (InstrumentType)this.symbolTypes.@get(this.symbol);
			}
			if (this.tradeMessages)
			{
				this.OnTrade(info);
			}
			if (this.bboMessages)
			{
				this.OnQuote(info);
			}
			this.raw.ReleaseItem(info);
		}

		protected abstract bool HasSupportedType(InstrumentFilter iFilter);

		private static bool IsBar(FeedFilter filter)
		{
			bool flag;
			long messageTypeMask = (long)0;
			InstrumentFilter[] typeFilters = filter.getTypeFilters();
			for (int i = 0; i < (int)typeFilters.Length; i++)
			{
				InstrumentFilter instrumentFilter = typeFilters[i];
				if (!instrumentFilter.isRestricted())
				{
					messageTypeMask = messageTypeMask | instrumentFilter.getMessageTypeMask();
				}
			}
			if ((messageTypeMask & (long)(1 << (MarketMessage.Type.TYPE_BAR.ordinal() & 31))) == (long)0)
			{
				flag = false;
			}
			else
			{
				if ((messageTypeMask & (long)(1 << (MarketMessage.Type.TYPE_TRADE.ordinal() & 31) | 1 << (MarketMessage.Type.TYPE_BBO.ordinal() & 31))) != (long)0)
				{
					throw new java.lang.UnsupportedOperationException("Simultaneous request for bars and ticks is not supported.");
				}
				flag = true;
			}
			return flag;
		}

		public bool isConnected()
		{
			return this.loggedIn;
		}

		private static bool IsFilterEmpty(FeedFilter filter)
		{
			bool flag;
			InstrumentType[] supportedInstruments = FeedFilter.getSupportedInstruments();
			int num = 0;
			while (true)
			{
				if (num < (int)supportedInstruments.Length)
				{
					CharSequenceSet symbols = filter.getSymbols(supportedInstruments[num]);
					if ((symbols == null ? true : symbols.isEmpty()))
					{
						num++;
					}
					else
					{
						flag = false;
						break;
					}
				}
				else
				{
					flag = true;
					break;
				}
			}
			return flag;
		}

		protected virtual void OnQuote(DepthMarketDataInfo info)
		{
		}

		private void OnTrade(DepthMarketDataInfo info)
		{
			//FeedProvider.LOGGER.log(Level.WARNING, string.Concat(new object[] { "VTS received trade:", info.Symbol, ", price", info.MatchPrice, ", size:", info.MatchQty, ", total size:", info.TotalMatchQty, ", time: ", info.MatchTime }));
			HashMap hashMap = (HashMap)this.tradeMap.@get(this.tradingDay);
			if (hashMap == null)
			{
				hashMap = new HashMap();
				this.tradeMap.put(this.tradingDay.toString(), hashMap);
			}
			FeedProvider.SymbolInfo symbolInfo = (FeedProvider.SymbolInfo)hashMap.@get(this.symbol);
			if (symbolInfo == null)
			{
				symbolInfo = new FeedProvider.SymbolInfo()
				{
					Volume = -1
				};
				hashMap.put(this.symbol.toString(), symbolInfo);
			}
			if ((double.IsNaN(info.TotalMatchQty) || info.TotalMatchQty == 0 ? true : symbolInfo.Volume != info.TotalMatchQty))
			{
				double matchQty = info.MatchQty;
				symbolInfo.Volume = info.TotalMatchQty;
				if (matchQty > 0)
				{
					DateTime dateTime = DateTime.ParseExact(string.Concat(info.TradingDay, info.MatchTime), "yyyyMMddHHmmssffffff", CultureInfo.InvariantCulture, DateTimeStyles.None);
					dateTime = dateTime.AddHours(-8);
					this.trade.originalTimestamp = DateConverter.ToLong(dateTime);
					this.trade.symbol = this.symbol;
					this.trade.price = info.MatchPrice;
					this.trade.size = matchQty;
					this.trade.exchangeCode = this.exchangeCode;
					this.trade.instrumentType = this.instrumentType;
					this.receiver.send(this.trade);
				}
			}
			else
			{
				//FeedProvider.LOGGER.log(Level.WARNING, string.Concat(new object[] { "VTS filter trade:", info.Symbol, ", price", info.MatchPrice, ", size:", info.MatchQty, ", total size:", info.TotalMatchQty, ", time: ", info.MatchTime }));
			}
		}

		private void ProcessEvents()
		{
			while (true)
			{
				try
				{
					this.HandleEvents((DepthMarketDataInfo)this.msgQueue.take());
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					if ((exception is ThreadAbortException || exception is ThreadInterruptedException || exception is java.lang.InterruptedException ? false : !(exception is UncheckedInterruptedException)))
					{
						FeedProvider.LOGGER.log(Level.SEVERE, string.Concat("Unexpected exception in order event processor: ", exception.Message), exception);
						throw;
					}
					else
					{
						FeedProvider.LOGGER.log(Level.WARNING, "Order event processor thread has been interrupted!");
						break;
					}
				}
			}
		}

		private void ProcessMessageTypes(FeedFilter filter)
		{
			bool flag;
			bool flag1;
			bool flag2;
			bool flag3;
			if ((filter == null ? false : !filter.isRestricted()))
			{
				flag = true;
				flag1 = false;
				flag2 = false;
				flag3 = false;
				InstrumentType[] supportedInstruments = FeedFilter.getSupportedInstruments();
				for (int i = 0; i < (int)supportedInstruments.Length; i++)
				{
					InstrumentFilter instrumentFilter = filter.getFilter(supportedInstruments[i]);
					if ((instrumentFilter == null || instrumentFilter.isRestricted() ? false : this.HasSupportedType(instrumentFilter)))
					{
						flag1 = instrumentFilter.acceptMessageType(MarketMessage.Type.TYPE_TRADE);
						flag2 = instrumentFilter.acceptMessageType(MarketMessage.Type.TYPE_BBO);
						if (flag)
						{
							goto Label1;
						}
						if ((this.tradeMessages != flag1 || this.bboMessages != flag2 ? true : this.level2Messages != flag3))
						{
							throw new System.Exception("Incorrect feed filter: different instrument types subscribed to different message types.");
						}
					}
				}
			}
			return;
		Label1:
			this.tradeMessages = flag1;
			this.bboMessages = flag2;
			this.level2Messages = flag3;
			flag = false;
		}

		public void removeDisconnectEventListener(DisconnectEventListener listener)
		{
			this.disconnectableImpl.removeDisconnectEventListener(listener);
		}

		public override void setFilter(FeedFilter filter)
		{
			if ((filter == null || filter.isRestricted() ? false : FeedProvider.IsBar(filter)))
			{
				throw new java.lang.UnsupportedOperationException("Live feed for bars is not supported.");
			}
			this.filter = filter;
			this.ApplyFilter();
		}

		private void Subscribe(string[] symbols)
		{
			if (this.loggedIn)
			{
				try
				{
					if (this.adapter != null)
					{
						this.adapter.Subscribe(symbols);
					}
				}
				catch (Exception exception)
				{
					FeedProvider.LOGGER.log(Level.SEVERE, "Unable to subscribe to symbols.", exception);
				}
			}
		}

		private void Unsubscribe(string[] symbols)
		{
			if (this.loggedIn)
			{
				try
				{
					if (this.adapter != null)
					{
						this.adapter.Unsubscribe(symbols);
					}
				}
				catch (Exception exception)
				{
					FeedProvider.LOGGER.log(Level.SEVERE, "Unable to unsubscribe from symbols.", exception);
				}
			}
		}

		private void UnsubscribeAll()
		{
			if (this.loggedIn)
			{
				try
				{
					if (this.adapter != null)
					{
						this.adapter.Unsubscribe(this.subscription.ToArray<string>());
					}
				}
				catch (Exception exception)
				{
					FeedProvider.LOGGER.log(Level.SEVERE, "Unable to unsubscribe from all symbols.", exception);
				}
			}
		}

		private class SymbolInfo
		{
			private double @value;

			public double Volume
			{
				get
				{
					return this.@value;
				}
				set
				{
					this.@value = value;
				}
			}

			public SymbolInfo() : this(0)
			{
			}

			public SymbolInfo(double value)
			{
				this.@value = value;
			}
		}
	}
}