using AxYuantaQuoteLib;
using java.util.logging;
using System;
using System.Windows.Forms;

namespace Vegasoul.Data.Yuanta
{
	internal abstract class YuantaAdapter
	{
		protected YuantaConfig config;

		protected AxYuantaQuote yuantaQuoteAPI;

		private YuantaQuoteForm yuantaQuoteForm;

		protected readonly static Logger LOGGER;

		protected bool isActive;

		static YuantaAdapter()
		{
			YuantaAdapter.LOGGER = Logger.getLogger("vegasoul.yuanta.provider");
		}

		public YuantaAdapter(YuantaConfig config)
		{
			this.config = config;
			this.yuantaQuoteForm = new YuantaQuoteForm();
			this.yuantaQuoteForm.Show();
			while (!this.yuantaQuoteForm.Loaded)
			{
			}
			this.yuantaQuoteAPI = this.yuantaQuoteForm.YuantaQuoteAPI;
		}
	}
}