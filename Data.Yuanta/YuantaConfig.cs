using System;

namespace Vegasoul.Data.Yuanta
{
	internal class YuantaConfig
	{
		private string quantServerHomeDir;

		private string serverIP;

		private string serverPort;

		private string userId;

		private string password;

		private int timezoneShift;

		private string updateMode;

		private int loginTimeout;

		public long IntervalBetweenReconnections
		{
			get
			{
				return (long)1000;
			}
		}

		public int LoginTimeout
		{
			get
			{
				return this.loginTimeout;
			}
		}

		public string Password
		{
			get
			{
				return this.password;
			}
		}

		public string QuantServerHomeDir
		{
			get
			{
				return this.quantServerHomeDir;
			}
		}

		public string ServerIP
		{
			get
			{
				return this.serverIP;
			}
		}

		public string ServerPort
		{
			get
			{
				return this.serverPort;
			}
		}

		public int TimezoneShift
		{
			get
			{
				return this.timezoneShift;
			}
		}

		public string UpdateMode
		{
			get
			{
				return this.updateMode;
			}
		}

		public string UserId
		{
			get
			{
				return this.userId;
			}
		}

		public YuantaConfig(string quantServerHomeDir, string serverIP, string serverPort, string userId, string password, int timezoneShift, int loginTimeout)
		{
			this.quantServerHomeDir = quantServerHomeDir;
			this.serverIP = serverIP;
			this.serverPort = serverPort;
			this.userId = userId;
			this.password = password;
			this.timezoneShift = timezoneShift;
			this.updateMode = "4";
			this.loginTimeout = loginTimeout;
		}
	}
}